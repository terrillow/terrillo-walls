<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title><?php echo page_title('Page can’t be found'); ?> - <?php echo site_name(); ?></title>

	<meta name="description" content="<?php echo site_description(); ?>">

	<link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="<?php echo theme_url('/css/reset.css'); ?>">
	<link rel="stylesheet" href="<?php echo theme_url('/css/style.min.css'); ?>">
	<link rel="stylesheet" href="<?php echo theme_url('/css/small.css'); ?>" media="(max-width: 400px)">
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

	<link rel="alternate" type="application/rss+xml" title="RSS" href="<?php echo rss_url(); ?>">
	<link rel="shortcut icon" href="<?php echo theme_url('img/ficon.png'); ?>">

	<!--[if lt IE 9]>
		<script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<script>var base = '<?php echo theme_url(); ?>';</script>

    <meta name="viewport" content="width=device-width">
    <meta name="generator" content="Anchor CMS">

    <meta property="og:title" content="<?php echo site_name(); ?>">
    <meta property="og:type" content="website">
    <meta property="og:url" content="<?php echo e(current_url()); ?>">
    <meta property="og:image" content="<?php echo theme_url('img/og_image.gif'); ?>">
    <meta property="og:site_name" content="<?php echo site_name(); ?>">
    <meta property="og:description" content="<?php echo site_description(); ?>">

	<?php if(customised()): ?>
	    <!-- Custom CSS -->
		<style><?php echo article_css(); ?></style>

		<!--  Custom Javascript -->
		<script><?php echo article_js(); ?></script>
	<?php endif; ?>
</head>
<body class="<?php echo body_class(); ?>">

	<header id="top">
		<a href="<?php echo base_url(); ?>"><h1><?php echo site_name(); ?></h1></a>
	</header>
	
	<form id="search" action="<?php echo search_url(); ?>" method="post">
		<label for="term">Search my blog:</label>
		<input type="search" id="term" name="term" placeholder="To search, type and hit enter&hellip;" value="<?php echo search_term(); ?>">
	</form>
	
	<div class="space"></div>
	