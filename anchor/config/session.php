<?php

return array(
	'driver' => 'database',
	'cookie' => 'anchorcms',
	'table' => 'cronix_terrillosessions',
	'lifetime' => 86400,
	'expire_on_close' => false,
	'path' => '/',
	'domain' => '',
	'secure' => false
);